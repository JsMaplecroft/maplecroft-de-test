import json
import os
from collections import defaultdict
from functools import lru_cache

from flask import Response
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource

DIR = os.path.dirname(os.path.abspath(__file__))
BIKE_MAP_PATH = os.path.join(DIR, '../../bikes.json')


@lru_cache()
def _load_bike_map():
    with open(BIKE_MAP_PATH, 'r') as f:
        locations = json.load(f)
    bike_map = defaultdict(list)
    for location in locations:
        for shape_id in location['shapeIDs']:
            bike_map[shape_id].append(location)
    return bike_map


class SiteList(Resource):
    method_decorators = [jwt_required()]

    def get(self):
        """ The api should accept a query parameter `?admin_area` and list all the sites
            within this admin area

        :return:
        """
        try:
            admin_area = request.args['admin_area']
        except KeyError:
            return Response('Expected admin_area query param', status=400)
        bike_map = _load_bike_map()
        if admin_area not in bike_map:
            return Response(f'Expected admin_area to be one of: {sorted(bike_map.keys())}', status=404)
        return bike_map[admin_area]
