import click
from flask.cli import with_appcontext


@click.group()
def cli():
    """Main entry point"""


@cli.command("init")
@with_appcontext
def init():
    """Create a new admin user"""
    from api.extensions import db
    from api.models import User

    click.echo("create user")
    user = User(username="admin", email="admin@mail.com", password="admin", active=True)
    db.session.add(user)
    db.session.commit()
    click.echo("created user admin")


@cli.command("load_sites")
@with_appcontext
def load_sites():
    """
    Download bike data from
    http://api.citybik.es/v2/networks

    Determine what admin area they are in by using the GeoBoundaries dataset

    For example the admin areas for Great Britain are:
    https://www.geoboundaries.org/gbRequest.html?ISO=GBR&ADM=ADM3

    """

    # TODO: Multi-threading / async
    import shapely.geometry
    import json
    from api.external_api import ApiCityBikes
    from api.external_api import ApiGeoBoundaries

    citybikes_api = ApiCityBikes()
    geo_bounaries_api = ApiGeoBoundaries()
    bikes_dataset = citybikes_api.get_all()

    shapes = []
    shapes.extend(geo_bounaries_api.get_shapes('ADM1'))
    shapes.extend(geo_bounaries_api.get_shapes('ADM2'))
    shapes.extend(geo_bounaries_api.get_shapes('ADM3'))

    for i, location in enumerate(bikes_dataset, start=1):
        print(f'Processed location {i} of {len(bikes_dataset)}')
        lat = location['location']['latitude']
        long = location['location']['longitude']
        location['shapeIDs'] = []

        for shape in shapes:
            _shape = shape['shape']
            if _shape.contains(shapely.geometry.Point(long, lat)):
                location['shapeIDs'].append(shape['shapeID'])

    with open('bikes.json', 'w') as f:
        json.dump(bikes_dataset, f, indent=4)


if __name__ == "__main__":
    cli()
