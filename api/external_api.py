import hashlib
import json
import os
import time
import urllib.error
import urllib.request

import shapely.geometry

DIR = os.path.dirname(os.path.abspath(__file__))
CACHE_DIRECTORY = '.cache'


class MissingBoundaryException(Exception):
    pass


class ApiCityBikes:
    """
    http://api.citybik.es/v2/

    Terms of Service:

        CityBikes is a free service.
        There's just one restriction: if your project is using this API you should indicate it on your app and website,
        linking the project page. If your project is using PyBikes you should also mention it. There's no need to put
        a huge 1k px banner on your website, just make a clear and noticeable statement about the source of the
        information that your project is using. Inform your users about it, let them find this data.

    """
    URL = 'http://api.citybik.es/v2/networks'

    def get_all(self):
        return json.loads(_get_url(self.URL))['networks']


class ApiGeoBoundaries:
    """
    https://www.geoboundaries.org

    Terms of Service:

        Copyright 2020 Runfola et al. This is an open access article distributed under the terms of the
        Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction
        in any medium, provided the original author and source are credited.
    """

    BASE_URL = 'https://www.geoboundaries.org/gbRequest.html?ISO=ALL&ADM={admin_level}&TYP=SSCGS'

    # TYP=SSCGS - Simplified Single Country Globally Standardized.
    # A version of geoBoundaries simplified data that has been clipped to the U.S. Department of State
    # boundary file, ensuring no contested boundaries or overlap in the dataset. This globally
    # standardized product may have gaps between countries.

    def get_shapes(self, admin_level: str):
        assert admin_level in ('ADM1', 'ADM2', 'ADM3')
        url = self.BASE_URL.format(admin_level=admin_level)
        boundary_list = json.loads(_get_url(url))
        ret = []
        for i, boundary in enumerate(boundary_list, start=1):
            boundary_id = boundary["boundaryID"]
            boundary_iso = boundary["boundaryISO"]
            boundary_url = boundary["gjDownloadURL"]
            print(f'Fetching boundary {i} of {len(boundary_list)}: '
                  f'boundaryID={boundary_id} boundary_iso={boundary_iso} url={boundary_url}')
            ret.extend(self._iter_boundary_shapes(boundary))
        return ret

    def _iter_boundary_shapes(self, boundary):
        boundary_url = boundary["gjDownloadURL"]
        try:
            geo_boundary = json.loads(_get_url(boundary_url))
        except urllib.error.HTTPError:
            print('No boundary for boundary_id={boundary_id} '
                  'boundary_iso={boundary_iso}')
            return

        assert geo_boundary['type'] == 'FeatureCollection'
        print(f'Found {len(geo_boundary["features"])} shapes for boundaryId={boundary["boundaryID"]}')
        for feature in geo_boundary['features']:
            assert feature['type'] == 'Feature'
            geometry = feature['geometry']
            _shape = shapely.geometry.shape(geometry)  # shape is (long, lat) order
            # print(feature['properties'])
            polygon = {**feature['properties'], **{'shape': _shape, **boundary}}
            yield polygon


def _get_url(url, ttl=604800) -> bytes:
    # Save bandwidth by caching external API call responses on disk
    cache_key = url + str(int(time.time()) // ttl)
    cache_key_hash = hashlib.md5(cache_key.encode()).hexdigest()
    cache_path = os.path.join(DIR, CACHE_DIRECTORY, cache_key_hash)
    if os.path.isfile(cache_path):
        print(f'Found url={url} in cache. cache_file={cache_key_hash}')
        with open(cache_path, 'rb') as f:
            return f.read()
    print(f'Fetching URL: {url}')
    f = urllib.request.urlopen(url)
    assert f.status == 200
    body = f.read()
    assert isinstance(body, bytes)
    os.makedirs(os.path.dirname(cache_path), exist_ok=True)
    with open(cache_path, 'wb') as f:
        f.write(body)
    return body